= MathDSL: Un DSL para Apache Commons Math
Jorge Aguilera <jorge.aguilera@puravida-software.com>

== Apache Commons Math


== Matrices

Esto es un ejemplo extraido de la página oficial de Apache Commons math para multiplicar matrices

[source,java]
----
// Create a real matrix with two rows and three columns, using a factory
// method that selects the implementation class for us.
double[][] matrixData = { {1d,2d,3d}, {2d,5d,3d}};
RealMatrix m = MatrixUtils.createRealMatrix(matrixData);

// One more with three rows, two columns, this time instantiating the
// RealMatrix implementation class directly.
double[][] matrixData2 = { {1d,2d}, {2d,5d}, {1d, 7d}};
RealMatrix n = new Array2DRowRealMatrix(matrixData2);

// Note: The constructor copies  the input double[][] array in both cases.

// Now multiply m by n
RealMatrix p = m.multiply(n);
System.out.println(p.getRowDimension());    // 2
System.out.println(p.getColumnDimension()); // 2
----

*La idea de MathDSL es simplificar la sintáxis creando un DSL apropiado*


Por ejemplo, con MathDSL para declarar una matriz basta con:

[source,groovy]
----
Matriz m = matriz{  //<1>
    r 1, 2, 3       //<2>
    r 3, 2, 1
}
----
<1> import static com.puravida.math.matriz.MatrizDSL.matriz
<2> cada fila de la matriz comienza por la palabra reservada `r` (row)

=== Suma

[source,groovy]
----
def A = matriz {
    r 1,  3
    r 2,  3
    r 22, 33
}

def B = matriz{
    r 1, 3
    r 2, 3
    r 3, 1
}

def C = A+B

assert C == matriz {
    r 2, 6
    r 4, 6
    r 25, 34
}
----

=== Producto

[source,groovy]
----
def A = matriz {
    r 1,  3
    r 2,  3
    r 22, 33
}

def B = matriz{
    r 1, 3
    r 2, 3
    r 3, 1
}

def C = A*B

assert C == matriz {
    r 7, 12, 12
    r 8, 15, 15
    r 88, 165, 165
}
----

