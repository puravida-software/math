package com.puravida.math.matriz

import spock.lang.Specification
import static com.puravida.math.matriz.MatrizDSL.matriz

class MatrizDSLSpec extends Specification{

    def "define"(){
        given:
        Matriz m = matriz{
            r 1, 2, 3
            r 3, 2, 1
        }

        expect:
        m.m == 2
        m.n == 3
    }

    def "multiply"(){

        given: "a declaration"

        def A = matriz {
            r 1,  3
            r 2,  3
            r 22, 33
        }

        def B = matriz{
            r 1, 3, 3
            r 2, 3, 3
        }

        when:

        def C = A*B

        then:
        C.m == 3
        C.n == 3
        C == matriz {
            r 7, 12, 12
            r 8, 15, 15
            r 88, 165, 165
        }

    }

    def "plus"(){

        given: "a declaration"

        def A = matriz {
            r 1,  3
            r 2,  3
            r 22, 33
        }

        def B = matriz{
            r 1, 3
            r 2, 3
            r 3, 1
        }

        when:

        def C = A+B

        then:
        C.m == 3
        C.n == 2
        C == matriz {
            r 2, 6
            r 4, 6
            r 25, 34
        }

    }

}
