package com.puravida.math.matriz

class MatrizDSL {

    private List<double[]>values = []

    private MatrizDSL(){
    }

    private Matriz build(){
        Matriz.build(values as double[][])
    }

    static Matriz matriz(@DelegatesTo(MatrizDSL)Closure cl){
        MatrizDSL ret = new MatrizDSL()
        Closure clone = cl.rehydrate(ret,ret,ret)
        clone()
        ret.build()
    }

    MatrizDSL r( Number... row){
        values << ( row as double[] )
        this
    }

}
