package com.puravida.math.matriz

import groovy.transform.CompileStatic
import groovy.transform.TypeCheckingMode
import org.apache.commons.math3.linear.Array2DRowRealMatrix
import org.apache.commons.math3.linear.MatrixUtils
import org.apache.commons.math3.linear.RealMatrix


@CompileStatic
class Matriz {

    private RealMatrix matrix

    Matriz multiply(Matriz b){
        new Matriz(matrix: matrix*b.matrix)
    }

    Matriz plus(Matriz b){
        new Matriz(matrix: matrix.add(b.matrix))
    }


    boolean equals(Matriz b){
        matrix == b.matrix
    }

    String toString(){
        StringBuffer sb = new StringBuffer()
        matrix.data.each{ row->
            row.each{
                sb.append("$it\t")
            }
            sb.append('\n')
        }
        sb.toString()
    }

    protected static Matriz build(double[][] values){
        Matriz ret = new Matriz()
        ret.matrix = MatrixUtils.createRealMatrix(values)
        ret
    }

    private int m
    int getM(){
        matrix.rowDimension
    }

    private int n
    int getN(){
        matrix.columnDimension
    }
}
